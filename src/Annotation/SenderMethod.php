<?php

namespace Drupal\sender\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Annotation used to define sender method plugins.
 *
 * @Annotation
 */
class SenderMethod extends Plugin {
}
